# README #

### What is this repository for? ###
This extends the jupyter/scipy-notebook by adding the octave-kernel and additional dependencies to the docker image. 

### How to use the image? ###
For creating the container:
```
docker container create -p 8888:8888 --name octavenb sanketnaik/octave-notebook
```
For starting the same container: 
```
docker container start octavenb
```
##### How to login ? #####
As you might be already aware, all jupyter notebooks require a token to be able to login to the notebook. 
For being able to get the notebook, you should check the container logs:
```
docker container logs octavenb
```

### Docker compose ###

```
services:
  ...
  octavenb:
    image: sanketnaik/octave-notebook
	ports:
	  - "8888:8888"
  ...
```

And for checking the logs, for authentication token to login to notebook:
```
docker-compose logs octavenb
```