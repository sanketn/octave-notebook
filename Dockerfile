FROM jupyter/scipy-notebook
  
LABEL maintainer="Sanket Naik <sanketn@gmail.com>"
LABEL version="1.0"

USER root
RUN apt-get update && \
    apt-get install -y --no-install-recommends octave octave-symbolic octave-miscellaneous gnuplot ghostscript && \
    rm -rf /var/lib/apt/lists/*
USER $NB_UID

RUN conda install --quiet --yes \
    'octave_kernel' \
    && \
    conda clean --all -f -y